######################################################################
# This file copyright the Georgia Institute of Technology
#
# Permission is given to students to use or modify this file (only)
# to work on their assignments.
#
# You may NOT publish this file or make it available to others not in
# the course.
#
######################################################################


import math
from copy import deepcopy

# If you see different scores locally and on Gradescope this may be an indication
# that you are uploading a different file than the one you are executing locally.
# If this local ID doesn't match the ID on Gradescope then you uploaded a different file.
OUTPUT_UNIQUE_FILE_ID = False
if OUTPUT_UNIQUE_FILE_ID:
    import hashlib, pathlib

    file_hash = hashlib.md5(pathlib.Path(__file__).read_bytes()).hexdigest()
    print(f'Unique file ID: {file_hash}')


class DeliveryPlanner_PartA:
    """
    Required methods in this class are:
    
      plan_delivery(self, debug = False) which is stubbed out below.  
        You may not change the method signature as it will be called directly 
        by the autograder but you may modify the internals as needed.
    
      __init__: which is required to initialize the class.  Starter code is 
        provided that initializes class variables based on the definitions in
        testing_suite_partA.py.  You may choose to use this starter code
        or modify and replace it based on your own solution
    
    The following methods are starter code you may use for part A.  
    However, they are not required and can be replaced with your
    own methods.

      _search(self, debug=False): Where the bulk of the A* search algorithm
          could reside.  It should find an optimal path from the robot
          location to a goal.
          Hint:  you may want to structure this based
          on whether looking for a box or delivering a box.
  
    """

    ## Definitions taken from testing_suite_partA.py
    ORTHOGONAL_MOVE_COST = 2
    DIAGONAL_MOVE_COST = 3
    BOX_LIFT_COST = 4
    BOX_DOWN_COST = 2
    ILLEGAL_MOVE_PENALTY = 100

    def __init__(self, warehouse, robot_position, todo, box_locations):

        self.todo = todo
        self.current_todo = self.todo[0]  # need to cast str to int
        self.current_todo_iter = 0
        self.boxes_delivered = []
        self.total_cost = 0
        self.warehouse_viewer = warehouse
        self.box_locations = box_locations
        self.dropzone = self.robot_position = robot_position

        self.data = {}  # container to pass around with extra information
        self.action_indices = {}  # container with move indices

        # where the robot started
        self.initial_robot_position = self.robot_position

        # whether robot is carrying box
        self.is_holding_box = False

        # whether robot has spawned on or is located on drop zone
        self.is_on_dropzone = False

        self.delta = [[-1, 0],  # north
                      [0, -1],  # west
                      [1, 0],  # south
                      [0, 1],  # east
                      [-1, -1],  # northwest (diag)
                      [-1, 1],  # northeast (diag)
                      [1, 1],  # southeast (diag)
                      [1, -1]]  # southwest (diag)

        self.delta_directions = ["n", "w", "s", "e", "nw", "ne", "se", "sw"]

        self.move_commands = ["move n", "move w", "move s", "move e",
                              "move nw", "move ne", "move se", "move sw"]

        self.down_commands = ["down n", "down w", "down s", "down e",
                              "down nw", "down ne", "down se", "down sw"]

        # Can use this for a visual debug
        self.delta_name = ['^', '<', 'v', '>', '\\', '/', '[', ']']
        # You may choose to use arrows instead
        # self.delta_name = ['🡑', '🡐', '🡓', '🡒',  '🡔', '🡕', '🡖', '🡗']

        # Costs for each move
        self.delta_cost = [self.ORTHOGONAL_MOVE_COST,
                           self.ORTHOGONAL_MOVE_COST,
                           self.ORTHOGONAL_MOVE_COST,
                           self.ORTHOGONAL_MOVE_COST,
                           self.DIAGONAL_MOVE_COST,
                           self.DIAGONAL_MOVE_COST,
                           self.DIAGONAL_MOVE_COST,
                           self.DIAGONAL_MOVE_COST]


    def _search(self, start_position, debug=False, picking_up=False):
        """
        This method should be based on lesson modules for A*, see Search, Section 12-14.
        The bulk of the search logic should reside here, should you choose to use this starter code.
        Please condition any printout on the debug flag provided in the argument.  
        ***You may change this function signature (i.e. add arguments) as
        necessary***, except for the debug argument which must remain with a default of False
        """

        # this performs search for just a single action, e.g. picking up
        # or dropping off, so the function needs to be called sequentially


        # get a shortcut variable for the warehouse (note this is just a view no copying)
        warehouse = self.warehouse_viewer

        # Find and fill in the required moves per the instructions - example moves for test case 1
        moves = ['move w',
                 'move nw',
                 'lift 1',
                 'move se',
                 'down e',
                 'move ne',
                 'lift 2',
                 'down s']

        # initialize open list
        g = 0
        # x = self.initial_robot_position[0]
        # y = self.initial_robot_position[1]
        if start_position:
            x = start_position[0]
            y = start_position[1]
        else:
            start_position = []
            start_position[0] = self.initial_robot_position[0]
            start_position[1] = self.initial_robot_position[1]


        if picking_up:
            # goal = self.box_locations[int(self.current_todo)]
            goal = self.box_locations[self.todo[self.current_todo_iter]]
        else:
            goal = self.dropzone

        h = self.heuristic_L2(x, y, goal, debug=debug)
        f_initial = g + h

        # initialize final robot position list
        final_robot_position = []

        # append first element to open list
        self.data[(x, y)] = {}
        self.data[(x, y)]['f'] = f_initial
        self.data[(x, y)]['g'] = 0
        self.data[(x, y)]['h'] = 0

        # self.action_indices[(x, y)] = {}  # only need to allocate when we expand?

        # initialize movement commands list
        moves = []

        found = False
        failed = False

        while not found and not failed:
            # if open list is empty, then we failed search
            if len(self.data) == 0:
                failed = True
                break
                print("Failed search.")

            if not self.action_indices.get((x, y)):
                self.action_indices[(x, y)] = {}

            # we sort the list and find the lowest f-value node to expand
            # NOTE: for sorting, got inspiration from: https://stackoverflow.com/questions/68458576/how-do-i-sort-a-dictionary-by-a-subkey-in-python
            self.data = dict(sorted(self.data.items(), key=lambda x: x[1]['f'], reverse=True))
            next_node = self.data.popitem()
            if debug:
                print(f"Popping item from self data with smallest f-value: {next_node}")

            # self.action_indices[(x, y)] = {}  # TODO(Remove)

            # next_node is the node that we're taking it off the list!
            f_next = next_node[1]['f']
            h_next = next_node[1]['h']
            g_next = next_node[1]['g']  # no need to sort by g_next anymore
            x_next, y_next = next_node[0]

            # self.data[(x_next,y_next)] = {}  # create dict for this specific node

            # check if we're at the to-do box (goal) for pick-up
            if x_next == goal[0] and y_next == goal[1]:
                if debug:
                    print(f"We found the goal for to-do box #{self.todo[self.current_todo_iter]}! Finished search.")
                found = True
                continue

            # we're not done yet, so we expand the next element (smallest f-value) and add
            # valid neighbors to the open list
            for delta_index, dir in enumerate(self.delta):
                dx = dir[0]
                dy = dir[1]
                x2 = x_next + dx
                y2 = y_next + dy

                # check the node to see if it's navigable, and expand the open list/frontier
                # these nodes should not have been explored before
                if (warehouse.is_inbounds(x2, y2) and (warehouse[x2][y2] == '.' or warehouse[x2][y2] == '@'
                or warehouse[x2][y2] == self.todo[self.current_todo_iter]) and self.action_indices.get((x2, y2)) is None):

                    # if it's navigable, we consider it closed, so we create
                    # a dictionary for it, and allocate memory for the action indices
                    self.data[(x2, y2)] = {}
                    self.action_indices[(x2, y2)] = {}

                    g_addition = self.delta_cost[delta_index]
                    # increment cost for this node
                    if x2 == goal[0] and y2 == goal[1]:
                        if debug:
                            print("We've found the box in x2,y2 probing. Not yet popping off the list.")
                        # break
                        if picking_up:
                            g_addition = 4  # to pick up the box
                        else:
                            g_addition = 2  # to put down the box

                        g2 = g_next + g_addition
                        self.data[(x2, y2)]['g'] = g2

                        # calculate f-value using heuristic
                        h2 = self.heuristic_L2(x2, y2, goal)
                        self.data[(x2, y2)]['h'] = h2
                        f2 = h2 + g2
                        self.data[(x2, y2)]['f'] = f2

                        self.action_indices[(x2, y2)] = delta_index
                        # we've found the goal, end loop
                        found = True

                    g2 = g_next + g_addition
                    self.data[(x2, y2)]['g'] = g2

                    # calculate f-value using heuristic
                    h2 = self.heuristic_L2(x2, y2, goal)
                    self.data[(x2, y2)]['h'] = h2
                    f2 = h2 + g2
                    self.data[(x2, y2)]['f'] = f2

                    if debug:
                        print(f"Adding to open list: [{f2}, {h2}, {g2}, {x2}, {y2}]")

                    # store the direction we moved to get to this expanded node
                    # we use action index in an attempt to preserve the data without
                    # it getting popped
                    self.action_indices[(x2, y2)] = delta_index

        # now, we populate the policy for movement
        # and we also need to update the map and robot position
        if picking_up:
            if debug:
                print(f"We're picking up a box!")
            cur_x = goal[0]
            cur_y = goal[1]
            # while cur_x != self.initial_robot_position[0] and cur_y != self.initial_robot_position[1]:
            while cur_x != start_position[0] and cur_y != start_position[1]:
                x2 = cur_x - self.delta[self.action_indices[(cur_x, cur_y)]][0]
                y2 = cur_y - self.delta[self.action_indices[(cur_x, cur_y)]][1]
                if cur_x == goal[0] and cur_y == goal[1]:
                    moves.append(f'lift {self.todo[self.current_todo_iter]}')
                    # update the warehouse to make it available
                    self.warehouse_viewer[cur_x][cur_y] = '.'

                    # back-propogate motion just one step, to where the robot
                    # picked up the box, and set this as the output final position
                    final_robot_position = [x2, y2]
                else:
                    moves = [self.move_commands[self.action_indices[(cur_x, cur_y)]]] + moves
                cur_x = x2
                cur_y = y2

            # take care of the initial movement, but ignore starting node
            if cur_x != start_position[0] or cur_y != start_position[1]:
                moves = [self.move_commands[self.action_indices[(cur_x, cur_y)]]] + moves

        if not picking_up:
            if debug:
                print(f"We're dropping off a box!")
            # then we're dropping off box
            cur_x = self.dropzone[0]
            cur_y = self.dropzone[1]
            # while cur_x != self.initial_robot_position[0] and cur_y != self.initial_robot_position[1]:
            while cur_x != start_position[0] or cur_y != start_position[1]:
                x2 = cur_x - self.delta[self.action_indices[(cur_x, cur_y)]][0]
                y2 = cur_y - self.delta[self.action_indices[(cur_x, cur_y)]][1]

                if cur_x == goal[0] and cur_y == goal[1]:
                    moves.append(f'down {self.delta_directions[self.action_indices[(cur_x, cur_y)]]}')

                    # back-propogate motion just one step, to where the robot
                    # picked up the box, and set this as the output final position
                    final_robot_position = [x2, y2]
                else:
                    moves = [self.move_commands[self.action_indices[(cur_x, cur_y)]]] + moves
                cur_x = x2
                cur_y = y2

            # take care of the initial movement, but ignore starting node
            # if cur_x != start_position[0] or cur_y != start_position[1]:
            #     moves = [self.move_commands[self.action_indices[(cur_x, cur_y)]]] + moves

        # finally, clean up our data for the next piece-wise run
        self.data = {}
        self.action_indices = {}

        if debug:
            print(f"Moves to take for {self.current_todo_iter} iteration: {moves}")

        return moves, final_robot_position

    def heuristic_L2(self, x, y, box_location, debug=False) -> float:
        """
        Internal function to compute heuristic to the goal.
        We're using the L2-norm for now.
        :param x: row
        :param y: col
        :return: h(x,y) to goal
        """
        goal_x = box_location[0]
        goal_y = box_location[1]
        diff_x = abs(goal_x - x)
        diff_y = abs(goal_y - y)
        L2_norm: float = math.sqrt((diff_x ** 2) + (diff_y ** 2))

        if debug:
            print(f"L2 norm from [{x}, {y}] -> [{goal_x}, {goal_y}]: {L2_norm}")

        return L2_norm


    def plan_delivery(self, debug=False):
        """
        plan_delivery() is required and will be called by the autograder directly.  
        You may not change the function signature for it.
        Add logic here to find the moves.  You may use the starter code provided above
        in any way you choose, but please condition any printouts on the debug flag
        """

        # Find the moves - you may add arguments and change this logic but please leave
        # the debug flag in place and condition all printouts on it.

        # You may wish to break the task into one-way paths, like this:
        #
        #    moves_to_1   = self._search( ..., debug=debug )
        #    moves_from_1 = self._search( ..., debug=debug )
        #    moves_to_2   = self._search( ..., debug=debug )
        #    moves_from_2 = self._search( ..., debug=debug )
        #    moves        = moves_to_1 + moves_from_1 + moves_to_2 + moves_from_2
        #

        moves = []
        global output_position
        for i in range(len(self.todo)):
            # pick up
            if i == 0:
                # start case, feed in initial position
                moves_to, output_position = self._search(start_position=self.initial_robot_position, debug=debug,
                                                           picking_up=True)
            else:
                moves_to, output_position = self._search(start_position=output_position, debug=debug, picking_up=True)

            # and then drop off
            moves_from, output_position = self._search(start_position=output_position, debug=debug, picking_up=False)
            self.current_todo_iter += 1  # update only after we've dropped off box
            moves = moves + moves_to + moves_from

        # moves_to_1, output_position = self._search(start_position=self.initial_robot_position, debug=debug, picking_up=True)
        # moves_from_1, output_position = self._search(start_position=output_position, debug=debug, picking_up=False)
        # self.current_todo_iter += 1  # update only after we've dropped off box
        #
        # moves_to_2, output_position = self._search(start_position=output_position, debug=debug, picking_up=True)
        # moves_from_2, output_position = self._search(start_position=output_position, debug=debug, picking_up=False)
        # self.current_todo_iter += 1  # update only after we've dropped off box

        # If you use _search(), you may need to modify it to take some
        # additional arguments for starting location, goal location, and
        # whether to pick up or deliver a box.

        # moves, output_position = self._search(debug=debug)

        # concatenate all our moves together
        # moves = moves_to_1 + moves_from_1 + moves_to_2 + moves_from_2

        if debug:
            for i in range(len(moves)):
                print(moves[i])

        return moves


class DeliveryPlanner_PartB:
    """
    Required methods in this class are:

        plan_delivery(self, debug = False) which is stubbed out below.
        You may not change the method signature as it will be called directly
        by the autograder but you may modify the internals as needed.

        __init__: required to initialize the class.  Starter code is
        provided that initializes class variables based on the definitions in
        testing_suite_partB.py.  You may choose to use this starter code
        or modify and replace it based on your own solution

    The following methods are starter code you may use for part B.
    However, they are not required and can be replaced with your
    own methods.

        _set_initial_state_from(self, warehouse): creates structures based on
            the warehouse and todo definitions and initializes the robot
            location in the warehouse

        _find_policy(self, goal, pickup_box=True, debug=False): Where the bulk 
            of the dynamic programming (DP) search algorithm could reside.  
            It should find an optimal path from the robot location to a goal.
            Hint:  you may want to structure this based
            on whether looking for a box or delivering a box.

    """

    # Definitions taken from testing_suite_partA.py
    ORTHOGONAL_MOVE_COST = 2
    DIAGONAL_MOVE_COST = 3
    BOX_LIFT_COST = 4
    BOX_DOWN_COST = 2
    ILLEGAL_MOVE_PENALTY = 100

    def __init__(self, warehouse, warehouse_cost, todo):

        self.todo = todo
        self.boxes_delivered = []
        self.total_cost = 0
        self._set_initial_state_from(warehouse)
        self.warehouse_cost = warehouse_cost

        self.delta = [[-1, 0],  # go up
                      [0, -1],  # go left
                      [1, 0],  # go down
                      [0, 1],  # go right
                      [-1, -1],  # up left (diag)
                      [-1, 1],  # up right (diag)
                      [1, 1],  # dn right (diag)
                      [1, -1]]  # dn left (diag)

        self.delta_directions = ["n", "w", "s", "e", "nw", "ne", "se", "sw"]

        # Use this for a visual debug
        self.delta_name = ['^', '<', 'v', '>', '\\', '/', '[', ']']
        # You may choose to use arrows instead
        # self.delta_name = ['🡑', '🡐', '🡓', '🡒',  '🡔', '🡕', '🡖', '🡗']

        # Costs for each move
        self.delta_cost = [self.ORTHOGONAL_MOVE_COST,
                           self.ORTHOGONAL_MOVE_COST,
                           self.ORTHOGONAL_MOVE_COST,
                           self.ORTHOGONAL_MOVE_COST,
                           self.DIAGONAL_MOVE_COST,
                           self.DIAGONAL_MOVE_COST,
                           self.DIAGONAL_MOVE_COST,
                           self.DIAGONAL_MOVE_COST]

    # state parsing and initialization function
    def _set_initial_state_from(self, warehouse):
        """Set initial state.

        Args:
            warehouse(list(list)): the warehouse map.
        """
        rows = len(warehouse)
        cols = len(warehouse[0])

        self.warehouse_state = [[None for j in range(cols)] for i in range(rows)]
        self.dropzone = None
        self.boxes = dict()

        for i in range(rows):
            for j in range(cols):
                this_square = warehouse[i][j]

                if this_square == '.':
                    self.warehouse_state[i][j] = '.'

                elif this_square == '#':
                    self.warehouse_state[i][j] = '#'

                elif this_square == '@':
                    self.warehouse_state[i][j] = '@'
                    self.dropzone = (i, j)

                else:  # a box
                    box_id = this_square
                    self.warehouse_state[i][j] = box_id
                    self.boxes[box_id] = (i, j)

    def _find_policy(self, goal, pickup_box=True, debug=False):
        """
        This method should be based on lesson modules for Dynamic Programming,
        see Search, Section 15-19 and Problem Set 4, Question 5.  The bulk of
        the logic for finding the policy should reside here should you choose to
        use this starter code.  Please condition any printout on the debug flag
        provided in the argument. You may change this function signature
        (i.e. add arguments) as necessary, except for the debug argument which
        must remain with a default of False
        """

        ##############################################################################
        # insert code in this method if using the starter code we've provided
        ##############################################################################

        # get a shortcut variable for the warehouse (note this is just a view it does not make a copy)
        grid = self.warehouse_state
        grid_costs = self.warehouse_cost

        # You will need to fill in the algorithm here to find the policy
        # The following are what your algorithm should return for test case 1
        if pickup_box:
            # To box policy
            policy = [['B', 'lift 1', 'move w'],
                      ['lift 1', '-1', 'move nw'],
                      ['move n', 'move nw', 'move n']]

        else:
            # Deliver policy
            policy = [['move e', 'move se', 'move s'],
                      ['move ne', '-1', 'down s'],
                      ['move e', 'down e', 'move n']]

        return policy

    def plan_delivery(self, debug=False):
        """
        plan_delivery() is required and will be called by the autograder directly.  
        You may not change the function signature for it.
        Add logic here to find the policies:  First to the box from any grid position
        then to the dropzone, again from any grid position.  You may use the starter
        code provided above in any way you choose, but please condition any printouts
        on the debug flag
        """
        ###########################################################################
        # Following is an example of how one could structure the solution using
        # the starter code we've provided.
        ###########################################################################

        # Start by finding a policy to direct the robot to the box from any grid position
        # The last command(s) in this policy will be 'lift 1' (i.e. lift box 1)
        goal = self.boxes['1']
        to_box_policy = self._find_policy(goal, pickup_box=True, debug=debug)

        # Now that the robot has the box, transition to the deliver policy.  The
        # last command(s) in this policy will be 'down x' where x = the appropriate
        # direction to set the box into the dropzone
        goal = self.dropzone
        deliver_policy = self._find_policy(goal, pickup_box=False, debug=debug)

        if debug:
            print("\nTo Box Policy:")
            for i in range(len(to_box_policy)):
                print(to_box_policy[i])

            print("\nDeliver Policy:")
            for i in range(len(deliver_policy)):
                print(deliver_policy[i])

        return (to_box_policy, deliver_policy)


class DeliveryPlanner_PartC:
    """
    Required methods in this class are:

        plan_delivery(self, debug = False) which is stubbed out below.
        You may not change the method signature as it will be called directly
        by the autograder but you may modify the internals as needed.

        __init__: required to initialize the class.  Starter code is
        provided that initializes class variables based on the definitions in
        testing_suite_partC.py.  You may choose to use this starter code
        or modify and replace it based on your own solution

    The following methods are starter code you may use for part C.
    However, they are not required and can be replaced with your
    own methods.

        _set_initial_state_from(self, warehouse): creates structures based on
            the warehouse and todo definitions and initializes the robot
            location in the warehouse

        _find_policy(self, goal, pickup_box=True, debug=False): 
            Where the bulk of your algorithm could reside.
            It should find an optimal policy to a goal.
            Remember that actions are stochastic rather than deterministic.
            Hint:  you may want to structure this based
            on whether looking for a box or delivering a box.

    """

    # Definitions taken from testing_suite_partA.py
    ORTHOGONAL_MOVE_COST = 2
    DIAGONAL_MOVE_COST = 3
    BOX_LIFT_COST = 4
    BOX_DOWN_COST = 2
    ILLEGAL_MOVE_PENALTY = 100

    def __init__(self, warehouse, warehouse_cost, todo, stochastic_probabilities):

        self.todo = todo
        self.boxes_delivered = []
        self._set_initial_state_from(warehouse)
        self.warehouse_cost = warehouse_cost
        self.stochastic_probabilities = stochastic_probabilities

        self.delta = [
            [-1, 0],  # go up
            [-1, -1],  # up left (diag)
            [0, -1],  # go left
            [1, -1],  # dn left (diag)
            [1, 0],  # go down
            [1, 1],  # dn right (diag)
            [0, 1],  # go right
            [-1, 1],  # up right (diag)]
        ]

        self.delta_directions = ["n", "nw", "w", "sw", "s", "se", "e", "ne"]

        # Use this for a visual debug
        self.delta_name = ['🡑', '🡔', '🡐', '🡗', '🡓', '🡖', '🡒', '🡕']

        # Costs for each move
        self.delta_cost = [self.ORTHOGONAL_MOVE_COST, self.DIAGONAL_MOVE_COST,
                           self.ORTHOGONAL_MOVE_COST, self.DIAGONAL_MOVE_COST,
                           self.ORTHOGONAL_MOVE_COST, self.DIAGONAL_MOVE_COST,
                           self.ORTHOGONAL_MOVE_COST, self.DIAGONAL_MOVE_COST, ]

    # state parsing and initialization function
    def _set_initial_state_from(self, warehouse):
        """Set initial state.

        Args:
            warehouse(list(list)): the warehouse map.
        """
        rows = len(warehouse)
        cols = len(warehouse[0])

        self.warehouse_state = [[None for j in range(cols)] for i in range(rows)]
        self.dropzone = None
        self.boxes = dict()

        for i in range(rows):
            for j in range(cols):
                this_square = warehouse[i][j]

                if this_square == '.':
                    self.warehouse_state[i][j] = '.'

                elif this_square == '#':
                    self.warehouse_state[i][j] = '#'

                elif this_square == '@':
                    self.warehouse_state[i][j] = '@'
                    self.dropzone = (i, j)

                else:  # a box
                    box_id = this_square
                    self.warehouse_state[i][j] = box_id
                    self.boxes[box_id] = (i, j)

    def _find_policy(self, goal, pickup_box=True, debug=False):
        """
        You are free to use any algorithm necessary to complete this task.
        Some algorithms may be more well suited than others, but deciding on the
        algorithm will allow you to think about the problem and understand what
        tools are (in)adequate to solve it. Please condition any printout on the
        debug flag provided in the argument. You may change this function signature
        (i.e. add arguments) as necessary, except for the debug argument which
        must remain with a default of False
        """

        ##############################################################################
        # insert code in this method if using the starter code we've provided
        ##############################################################################

        # get a shortcut variable for the warehouse (note this is just a view it does not make a copy)
        grid = self.warehouse_state
        grid_costs = self.warehouse_cost

        # You will need to fill in the algorithm here to find the policy
        # The following are what your algorithm should return for test case 1
        if pickup_box:
            # To-box policy
            # the below policy is hard coded to work for test case 1
            policy = [
                ['B', 'lift 1', 'move w'],
                ['lift 1', -1, 'move nw'],
                ['move n', 'move nw', 'move n'],
            ]

        else:
            # to-zone policy
            # the below policy is hard coded to work for test case 1
            policy = [
                ['move e', 'move se', 'move s'],
                ['move se', -1, 'down s'],
                ['move e', 'down e', 'move n'],
            ]

        return policy

    def plan_delivery(self, debug=False):
        """
        plan_delivery() is required and will be called by the autograder directly.
        You may not change the function signature for it.
        Add logic here to find the policies:  First to the box from any grid position
        then to the dropzone, again from any grid position.  You may use the starter
        code provided above in any way you choose, but please condition any printouts
        on the debug flag
        """
        ###########################################################################
        # Following is an example of how one could structure the solution using
        # the starter code we've provided.
        ###########################################################################

        # Start by finding a policy to direct the robot to the box from any grid position
        # The last command(s) in this policy will be 'lift 1' (i.e. lift box 1)
        goal = self.boxes['1']
        to_box_policy = self._find_policy(goal, pickup_box=True, debug=debug)

        # Now that the robot has the box, transition to the deliver policy.  The
        # last command(s) in this policy will be 'down x' where x = the appropriate
        # direction to set the box into the dropzone
        goal = self.dropzone
        to_zone_policy = self._find_policy(goal, pickup_box=False, debug=debug)

        if debug:
            print("\nTo Box Policy:")
            for i in range(len(to_box_policy)):
                print(to_box_policy[i])

            print("\nDeliver Policy:")
            for i in range(len(to_zone_policy)):
                print(to_zone_policy[i])

        # For debugging purposes you may wish to return values associated with each policy.
        # Replace the default values of None with your grid of values below and turn on the
        # VERBOSE_FLAG in the testing suite.
        to_box_values = None
        to_zone_values = None
        return (to_box_policy, to_zone_policy, to_box_values, to_zone_values)


def who_am_i():
    # Please specify your GT login ID in the whoami variable (ex: jsmith222).
    whoami = 'bzhao301'
    return whoami


if __name__ == "__main__":
    """ 
    You may execute this file to develop and test the search algorithm prior to running 
    the delivery planner in the testing suite.  Copy any test cases from the
    testing suite or make up your own.
    Run command:  python warehouse.py
    """

    # Test code in here will NOT be called by the autograder
    # This section is just a provided as a convenience to help in your development/debugging process

    # Testing for Part A
    # testcase 1
    print('\nTesting for part A:')

    from testing_suite_partA import wrap_warehouse_object, Counter

    # test case data starts here
    warehouse = [
        '....',
        '.1#2',
        '..#.',
        '...@',
    ]
    todo = list('12')
    benchmark_cost = 23
    viewed_cell_count_threshold = 12
    robot_position = (3,3)
    box_locations = {
        '1': (1,1),
        '2': (1,3),
    }
    # test case data ends here

    viewed_cells = Counter()
    warehouse_access = wrap_warehouse_object(warehouse, viewed_cells)
    partA = DeliveryPlanner_PartA(warehouse_access, robot_position, todo, box_locations)
    partA.plan_delivery(debug=True)
    # Note that the viewed cells for the hard coded solution provided
    # in the initial template code will be 0 because no actual search
    # process took place that accessed the warehouse
    print('Viewed Cells:', len(viewed_cells))
    print('Viewed Cell Count Threshold:', viewed_cell_count_threshold)

    # Testing for Part B
    # testcase 1
    print('\nTesting for part B:')
    warehouse = ['1..',
                 '.#.',
                 '..@']

    warehouse_cost = [[3, 5, 2],
                      [10, math.inf, 2],
                      [2, 10, 2]]

    todo = ['1']

    partB = DeliveryPlanner_PartB(warehouse, warehouse_cost, todo)
    partB.plan_delivery(debug=True)

    # Testing for Part C
    # testcase 1
    print('\nTesting for part C:')
    warehouse = ['1..',
                 '.#.',
                 '..@']

    warehouse_cost = [[13, 5, 6],
                      [10, math.inf, 2],
                      [2, 11, 2]]

    todo = ['1']

    stochastic_probabilities = {
        'as_intended': .70,
        'slanted': .1,
        'sideways': .05,
    }

    partC = DeliveryPlanner_PartC(warehouse, warehouse_cost, todo, stochastic_probabilities)
    partC.plan_delivery(debug=True)
