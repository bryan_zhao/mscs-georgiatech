######################################################################
# This file copyright the Georgia Institute of Technology
#
# Permission is given to students to use or modify this file (only)
# to work on their assignments.
#
# You may NOT publish this file or make it available to others not in
# the course.
#
######################################################################

#These import statements give you access to library functions which you may
# (or may not?) want to use.
from math import *
import random
from body import *
from solar_system import *

# PROJECT CONSTANTS
MASS_EARTH = 5.97219e24  # from testing_suite_full.py


# user-defined data structures, define all particles
# other = {}  # contains all data integrated over time

# N_init = 2000  # number of particles
N_init = 1000
MEASUREMENT_NOISE = 3.940227066209606e-08  # noise on the gravimeter, tunable parameter #TODO, change from fixed to scaling
# MEASUREMENT_NOISE = 3.940227066209606e-07  # noise on the gravimeter, tunable parameter #TODO, change from fixed to scaling
# MEASUREMENT_NOISE = 7e-07
TARGET_MASS = 0.02 * MASS_EARTH
# FUZZING_FACTOR = 0.05
# RELATIVE_MEASUREMENT_FACTOR = 0.07
FUZZING_FACTOR = 0.06
RELATIVE_MEASUREMENT_FACTOR = 0.07

def resample(particle_container, solar_system=None, measurement_noise=None):
    """Resamples a set of particles based on their importance weights. This
    is done via the sampling wheel method covered in Lecture 3.

    Note that this generates an entirely new dictionary which must match
    the format of the original dictionary."""

    apply_fuzzing = True

    # Inspired by https://stackoverflow.com/questions/44602613/sum-dictionary-values-in-python-by-specific-keys
    weights_list = [particle_container[particle_id]['weight'] for particle_id in particle_container]
    max_weight = max(weights_list)
    beta = 0.
    # index = int(random.random() * (N_init-1))  # initialize randomly drawn index
    index = int(random.random() * (len(particle_container)-1))
    other_updated = {}
    particle_id_updated = 0

    # run the sampling wheel
    for particle_id in particle_container:
        beta += random.random() * 2.0 * max_weight
        while beta > weights_list[index]:
            beta -= weights_list[index]
            index = (index+1) % N_init
        other_updated[particle_id_updated] = {}

        if apply_fuzzing:
            other_updated[particle_id_updated]['body'] = None
            x = particle_container[index]['body'].r[0]
            x += random.uniform(-FUZZING_FACTOR*AU, FUZZING_FACTOR*AU)
            y = particle_container[index]['body'].r[1]
            y += random.uniform(-FUZZING_FACTOR*AU, FUZZING_FACTOR*AU)
            other_updated[particle_id_updated]['body'] = Body.create_body_at_xy_in_orbit(r=(x,y), mass=TARGET_MASS, measurement_noise=measurement_noise,
                                                       mass_sun=solar_system.sun.mass)
        else:
            other_updated[particle_id_updated]['body'] = particle_container[index]['body']

        # other_updated[particle_id_updated]['timestep'] = particle_container[index]['timestep']  # copy over all fields

        particle_id_updated += 1  # increment new iterator for particle container


    return other_updated


def measurement_prob(Z, particle_measurement, measurement_noise):
    """Calculate the likelihood of observing a particular measurement
    assuming the satellite were located at the particle's position.
    Returns a float."""

    # measurement noise is sigma, and this
    # models a 1D Gaussian pdf
    likelihood = 1.0
    likelihood *= (1.0 / (measurement_noise**2. * sqrt(2.0*pi))) * exp((-1./2.) * ((particle_measurement-Z)/measurement_noise)**2.)
    if not isinstance(likelihood, float):
        raise TypeError("The likelihood should be returned as a float.")

    return likelihood

def generate_weights(particle_container: dict) -> None:
    """Takes in a dictionary with particles, and generates weights based on their
    computed likelihoods."""
    W_sum = 0.0
    err_threshold = 0.00001
    for particle_id in particle_container:
        likelihood = particle_container[particle_id]['likelihood']
        W_sum += likelihood

    if W_sum < err_threshold:
        W_sum = 0.1

    for particle_id in particle_container:
        if W_sum != 0:
            particle_container[particle_id]['weight'] = particle_container[particle_id]['likelihood']/W_sum
        else:
            raise ZeroDivisionError("W_sum is 0. Check the logic in generating weights.")
        # print(particle_container[particle_id]['weight'])

def compute_aggregation_mean(final_particle_container):

    x_sum = y_sum = 0
    for particle_id in final_particle_container:
        (x, y) = final_particle_container[particle_id]['body'].r
        x_sum += x
        y_sum += y
    x_mean = x_sum / len(final_particle_container)
    y_mean = y_sum / len(final_particle_container)

    return x_mean, y_mean


def get_optional_points_to_plot(particle_container):
    """Takes in a dict of particles, and outputs a list of (x, y, h)"""
    list = []
    for particle_id in particle_container:
        (x, y) = particle_container[particle_id]['body'].r
        h = get_heading(x, y)
        list.append((x, y, h))
    return list

def get_heading(x, y):
    """ From body.py, create_body_at_xy_in_orbit(cls, r, mass, measurement_noise, mass_sun)"""
    angle = atan2(y, x)
    heading = angle + pi / 2  # perpendicular

    return heading

# First, initialize 300 particles uniformly within the bounds of +/- 4 AU
# Then, compute the likelihood of each of the particles
# Weight the particles by their likelihood, with a relative scaling
# Resample the particles, and apply some fuzz
# Aggregate the estimate, e.g. select one (by doing a simple average for now)
# Return the data into the 'other' structure

def estimate_next_pos(solar_system, gravimeter_measurement, other=None):
    """
    Estimate the next (x,y) position of the satelite.
    This is the function you will have to write for part A.

    :param solar_system: SolarSystem
        A model of the positions, velocities, and masses
        of the planets in the solar system, as well as the sun.
    :param gravimeter_measurement: float
        A floating point number representing
        the measured magnitude of the gravitation pull of all the planets
        felt at the target satellite at that point in time.
    :param other: any
        This is initially None, but if you return an OTHER from
        this function call, it will be passed back to you the next time it is
        called, so that you can use it to keep track of important information
        over time. (We suggest you use a dictionary so that you can store as many
        different named values as you want.)
    :return:
        estimate: Tuple[float, float]. The (x,y) estimate of the target satellite at the next timestep
        other: any. Any additional information you'd like to pass between invocations of this function
        optional_points_to_plot: List[Tuple[float, float, float]].
            A list of tuples like (x,y,h) to plot for the visualization
    """

    use_relative_measurement_noise = True

    # handling first time step
    if other is None:
        other = {}
        for i in range(N_init):
            # 1. create a uniform distribution of N particles and initialize
            # them into the orbit
            x = random.uniform(-4 * AU, 4 * AU)
            y = random.uniform(-4 * AU, 4 * AU)
            other[i] = {}
            # initialize the body with an initial measurement noise value
            other[i]['body'] = Body.create_body_at_xy_in_orbit(r=(x,y), mass=TARGET_MASS, measurement_noise=MEASUREMENT_NOISE,
                                                       mass_sun=solar_system.sun.mass)
            other[i]['measurement'] = other[i]['body'].compute_gravity_magnitude(planets=solar_system.planets)
            if use_relative_measurement_noise:
                other[i]['likelihood'] = measurement_prob(Z=gravimeter_measurement,
                                                          particle_measurement=other[i]['measurement'],
                                                          measurement_noise=(RELATIVE_MEASUREMENT_FACTOR * other[i]['measurement']))
            else:
                other[i]['likelihood'] = measurement_prob(Z=gravimeter_measurement, particle_measurement=other[i]['measurement'],
                                                          measurement_noise=MEASUREMENT_NOISE)
            # other[i]['timestep'] = 1
        # 2. calculate weight of each particle
        generate_weights(other)

        # 3. resample the particles
        other = resample(particle_container=other, solar_system=solar_system)

        # for now, we will try resampling the same amount as N_init without any thresholding


        # 4. apply fuzzing


        # 5. mimick motion model for all particles
        for i in range(len(other)):
            other[i]['body'] = solar_system.move_body(other[i]['body'])

    # example of how to get the gravity magnitude at a body in the solar system:
    # particle = Body(r=[1*AU, 1*AU], v=[0, 0], mass=0, measurement_noise=0)
    # particle_gravimeter_measurement = particle.compute_gravity_magnitude(planets=solar_system.planets)

    if other is not None:
        for i in range(len(other)):
            # updated_other = {}  # new list with resampled particles
            # 1. store measurement
            other[i]['measurement'] = other[i]['body'].compute_gravity_magnitude(planets=solar_system.planets)

            # 2. compute likelihood for each particle
            if use_relative_measurement_noise:
                other[i]['likelihood'] = measurement_prob(Z=gravimeter_measurement,
                                                          particle_measurement=other[i]['measurement'],
                                                          measurement_noise=(RELATIVE_MEASUREMENT_FACTOR * other[i]['measurement']))
            else:
                other[i]['likelihood'] = measurement_prob(Z=gravimeter_measurement, particle_measurement=other[i]['measurement'],
                                                      measurement_noise=MEASUREMENT_NOISE)


            # update timestep for metadata
            # other[i]['timestep'] += 1

        # 2. after loop, calculate weight of each particle
        generate_weights(other)

        # 3. resample the particles
        other = resample(particle_container=other, solar_system=solar_system)

        # 4. mimick the motion model
        for i in range(len(other)):
            other[i]['body'] = solar_system.move_body(other[i]['body'])

    # compute the aggregation and select a single estimate
    xy_estimate = compute_aggregation_mean(other)

    # You may optionally also return a list of (x,y,h) points that you would like
    # the PLOT_PARTICLES=True visualizer to plot for visualization purposes.
    # If you include an optional third value, it will be plotted as the heading
    # of your particle.

    optional_points_to_plot = get_optional_points_to_plot(other)

    # optional_points_to_plot = [(1*AU, 1*AU), (2*AU, 2*AU), (3*AU, 3*AU)]  # Sample (x,y) to plot
    # optional_points_to_plot = [(1*AU, 1*AU, 0.5), (2*AU, 2*AU, 1.8), (3*AU, 3*AU, 3.2)]  # (x,y,heading)

    return xy_estimate, other, optional_points_to_plot


def next_angle(solar_system, gravimeter_measurement, other=None):
    """
    Gets the next angle at which to send out an sos message to the home planet,
    the last planet in the solar system's planet list.
    This is the function you will have to write for part B.

    The input parameters are exactly the same as for part A.

    :return:
        bearing: float. The absolute angle from the satellite to send an sos message
        estimate: Tuple[float, float]. The (x,y) estimate of the target satellite at the next timestep
        other: any. Any additional information you'd like to pass between invocations of this function
        optional_points_to_plot: List[Tuple[float, float, float]].
            A list of tuples like (x,y,h) to plot for the visualization
    """

    estimate, other, optional_points_to_plot = estimate_next_pos(solar_system, gravimeter_measurement, other)

    # The bearing angle will have to be between [-pi, pi]

    # At what angle to send an SOS message this timestep
    # bearing = 0.0
    # estimate = (110172640485.32968, -66967324464.19617)

    estimated_x = estimate[0]
    estimated_y = estimate[1]

    last_planet = solar_system.planets[-1]
    x_last_planet = last_planet.r[0]
    y_last_planet = last_planet.r[1]

    delta_x = x_last_planet - estimate[0]
    delta_y = y_last_planet - estimate[1]

    # heading = get_heading(estimated_x, estimated_y)
    # bearing = atan2(delta_y, delta_x) - heading
    bearing = atan2(delta_y, delta_x)

    # from testing_suite_full.py:
    bearing = max(-pi, bearing)
    bearing = min(bearing, pi)

    # You may optionally also return a list of (x,y) or (x,y,h) points that
    # you would like the PLOT_PARTICLES=True visualizer to plot.
    #
    # optional_points_to_plot = [ (1*AU,1*AU), (2*AU,2*AU), (3*AU,3*AU) ]  # Sample plot points
    # return bearing, estimate, other, optional_points_to_plot

    return bearing, estimate, other


def who_am_i():
    # Please specify your GT login ID in the whoami variable (ex: jsmith222).
    whoami = 'bzhao301'
    return whoami
