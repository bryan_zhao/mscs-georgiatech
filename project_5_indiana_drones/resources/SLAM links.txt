I have collected the following resource links over multiple semesters
teaching CS 7638 (AI4R) from those suggested by students on Piazza. The
contents of any particular URL may change or disappear over time, so I can
not fully endorse any particular link below, but you should find the majority
to be quite useful as external resources to assist your understanding.


READINGS

Dr Thrun's book “Probabilistic Robots” dives much deeper into the SLAM
algorithms including the mathematical proofs. 
Check out Chapter 11 for graph slam (and 10, 12, and 13 if you want a lot
more on other SLAM techniques)

EKF for Online SLAM - 
http://ais.informatik.uni-freiburg.de/teaching/ws12/mapping/pdf/slam04-ekf-slam.pdf

Many good Algorithms visualized well, including SLAM and FastSLAM:
https://atsushisakai.github.io/PythonRobotics/

FastSlam research paper:
https://ai.stanford.edu/~koller/Papers/Montemerlo+al:AAAI02.pdf


GraphSLAM tutorial based upon Udacity lectures:
https://web.archive.org/web/20200826085335/https://medium.com/@krunalkshirsagar/graph-slam-a-noobs-guide-to-simultaneous-localization-and-mapping-aaff4ee91dee


VIDEOS

A student found the SLAM videos from this course useful:
"After watching this video I get the idea of GraphSLAM as solving a 
over-determined linear equation system through the method of least-squares
error minimization."
See lecture 16.
http://ais.informatik.uni-freiburg.de/teaching/ss17/robotics/

For a very step-by-step reasoning of HOW this produces the correct output,
view this KahnAcademy explanation of how to represent a series of linear
equations as a matrix and the derivation of the formula Identity * column
vectorC = A inverse * column vector B at the end of the following video:
"Sal shows how a system of two linear equations can be represented with the
equation A*x=b where A is the coefficient matrix, x is the variable vector,
and b is the constant vector. Created by Sal Khan."
https://www.khanacademy.org/math/precalculus/precalc-matrices/solving-equations-with-inverse-matrices/v/matrix-equations-systems

SLAM-Course -  (2013/14; Cyrill Stachniss)
https://www.youtube.com/watch?v=U6vr3iNrwRA&list=PLgnQpQtFTOGQrZ4O5QzbIHgl3b1JHimN_
Cyrill Stachniss SLAM lectures 4 and 5 for EKF.
https://www.youtube.com/watch?v=DE6Jn2cB4J4
https://www.youtube.com/watch?v=XeWG5D71gC0
FastSLAM lecture 12:
https://www.youtube.com/watch?v=Tz3pg3d1TIo

