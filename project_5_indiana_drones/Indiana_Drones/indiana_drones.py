######################################################################
# This file copyright the Georgia Institute of Technology
#
# Permission is given to students to use or modify this file (only)
# to work on their assignments.
#
# You may NOT publish this file or make it available to others not in
# the course.
#
######################################################################

"""
 === Introduction ===

   The assignment is broken up into two parts.

   Part A:
        Create a SLAM implementation to process a series of landmark measurements (location of tree centers) and movement updates.
        The movements are defined for you so there are no decisions for you to make, you simply process the movements
        given to you.
        Hint: A planner with an unknown number of motions works well with an online version of SLAM.

    Part B:
        Here you will create the action planner for the drone.  The returned actions will be executed with the goal being to navigate to 
        and extract the treasure from the environment marked by * while avoiding obstacles (trees). 
        Actions:
            'move distance steering'
            'extract treasure_type x_coordinate y_coordinate' 
        Example Actions:
            'move 1 1.570963'
            'extract * 1.5 -0.2'

    Note: All of your estimates should be given relative to your drone's starting location.
    
    Details:
    - Start position
      - The drone will land at an unknown location on the map, however, you can represent this starting location
        as (0,0), so all future drone location estimates will be relative to this starting location.
    - Measurements
      - Measurements will come from trees located throughout the terrain.
        * The format is {'landmark id':{'distance':0.0, 'bearing':0.0, 'type':'D', 'radius':0.5}, ...}
      - Only trees that are within the horizon distance will return measurements. Therefore new trees may appear as you move through the environment.
    - Movements
      - Action: 'move 1.0 1.570963'
        * The drone will turn counterclockwise 90 degrees [1.57 radians] first and then move 1.0 meter forward.
      - Movements are stochastic due to, well, it being a robot.
      - If max distance or steering is exceeded, the drone will not move.
      - Action: 'extract * 1.5 -0.2'
        * The drone will attempt to extract the specified treasure (*) from the current location of the drone (1.5, -0.2).
      - The drone must be within 0.25 distance to successfully extract a treasure.

    The drone will always execute a measurement first, followed by an action.
    The drone will have a time limit of 10 seconds to find and extract all of the needed treasures.
"""

from typing import Dict, List
import numpy as np
from matrix import matrix
import math

# If you see different scores locally and on Gradescope this may be an indication
# that you are uploading a different file than the one you are executing locally.
# If this local ID doesn't match the ID on Gradescope then you uploaded a different file.
OUTPUT_UNIQUE_FILE_ID = False
if OUTPUT_UNIQUE_FILE_ID:
    import hashlib, pathlib
    file_hash = hashlib.md5(pathlib.Path(__file__).read_bytes()).hexdigest()
    print(f'Unique file ID: {file_hash}')

class SLAM:
    """Create a basic SLAM module.
    """

    def __init__(self):
        """Initialize SLAM components here.
        """

        # populate omega and xi with initial position
        self.omega: matrix = matrix()
        self.omega.zero(2, 2)  # store initial positions
        self.omega += matrix([[1, 0],
                              [0, 1]])
    # [[1, 0],
    #                                  0, 1]]) # np.array([[1, 0], [0, 1]])
    #     self.xi: matrix = np.array([0,
    #                         0])
        self.xi: matrix = matrix()
        self.xi.zero(2, 1)
        self.xi += matrix([[0],
                           [0]])
        self.mu: matrix = self.omega.inverse() * self.xi  # = np.matmul(np.linalg.inv(self.omega), self.xi)

        # store indices for omega and xi matrices
        self.k: int = 0  # position index
        self.l: int = 0  # landmark index

        # store data about the landmarks / trees
        self.landmark_history: dict = {}
        self.num_landmarks: int = 0

        # store information about drone's location
        self.x = 0
        self.y = 0
        self.robot_absolute_bearing = 0  # need to keep track of theta of drone over time!
        self.dx = 0
        self.dy = 0

        # sigma parameters for tuning
        self.movement_noise: float = 10
        self.measurement_noise: float = 2.5

    # Provided Functions
    def get_coordinates(self):
        """
        Retrieves the estimated (x, y) locations in meters of the drone and all landmarks (trees) when called.

        Args: None

        Returns:
            The (x,y) coordinates in meters of the drone and all landmarks (trees) in the format:
                    {
                        'self': (x, y),
                        '<landmark_id_1>': (x1, y1),
                        '<landmark_id_2>': (x2, y2),
                        ....
                    }
        """
        # print("getting coordinates!")

        self.mu: matrix = self.omega.inverse() * self.xi

        self.result = {}
        # extract information from mu for position
        drone_x = self.mu[0][0]
        drone_y = self.mu[1][0]
        self.result['self'] = (drone_x, drone_y)

        landmark_poses = self.mu[2:]

        for landmark_id in self.landmark_history:
            pose_index = self.landmark_history[landmark_id]['index'] * 2
            self.result[landmark_id] = (landmark_poses[pose_index][0], landmark_poses[pose_index+1][0])

        # extract information for all landmarks from class attributes
        # if self.landmark_history is not None:
        #     for landmark_id in self.landmark_history:
        #         # result[landmark_id] = (self.landmark_history[landmark_id]['x'], self.landmark_history[landmark_id]['y'])
        #         # result[landmark_id] = self.mu
        return self.result

    def process_measurements(self, measurements: Dict):
        """
        Process a new series of measurements and update (x,y) location of drone and landmarks

        Args:
            measurements: Collection of measurements of tree positions and radius
                in the format {'landmark id':{'distance': float <meters>, 'bearing':float <radians>, 'type': char, 'radius':float <meters>}, ...}

        """
        # landmark has landmark ID, distance, bearing, tree type, radius
        example_data = {281915587175935009285948130631518955326: {'distance': 2.947715633578139, 'bearing': 1.625351939310705, 'type': 'B', 'radius': 0.2}, 273218211129333906406540703699235893775: {'distance': 2.9441095102906765, 'bearing': 3.134425567274061, 'type': 'A', 'radius': 0.5}}
        for landmark_id in measurements:
            # this input tree bearing is in the absolute world frame
            tree_bearing = self.truncate_angle(measurements[landmark_id]['bearing'] + self.robot_absolute_bearing)

            # measurements are relative to the robot's current position and orientation
            Z_x = measurements[landmark_id]['distance'] * np.cos(tree_bearing) # + self.x
            Z_y = measurements[landmark_id]['distance'] * np.sin(tree_bearing) # + self.y

            # try relative measurements instead
            # Z_x = measurements[landmark_id]['distance'] * np.cos(measurements[landmark_id]['bearing'])
            # Z_y = measurements[landmark_id]['distance'] * np.sin(measurements[landmark_id]['bearing'])

            # if landmark has never been seen before, add it to the matrices and database
            if landmark_id not in self.landmark_history:
                self.landmark_history[landmark_id] = {}
                self.landmark_history[landmark_id]['x'] = Z_x
                self.landmark_history[landmark_id]['y'] = Z_y

                # add index to track where it is in omega and xi
                self.landmark_history[landmark_id]['index'] = self.num_landmarks
                self.landmark_history[landmark_id]['type'] = measurements[landmark_id]['type']
                self.landmark_history[landmark_id]['radius'] = measurements[landmark_id]['radius']

                ### NOTE: copied from ps6_answers.py ###
                assert (self.omega.dimx == self.omega.dimy)  # needs to be a square matrix
                dim = self.omega.dimx
                # idxs = [0, 1] + list(range(4, dim + 2))
                idxs = list(range(0, self.omega.dimx))  # here, keep everything, just pad the matrix
                self.omega = self.omega.expand(dim + 2, dim + 2, idxs, idxs)  # add to omega - Lx, Ly
                self.xi = self.xi.expand(dim + 2, 1, idxs, [0])  # add to xi - Lx, Ly for new landmarks
                ### END NOTE ###

                self.num_landmarks += 1  # increment for the next new landmark

            else:
                # otherwise, update the database with new measurement information
                self.landmark_history[landmark_id]['x'] = Z_x
                self.landmark_history[landmark_id]['y'] = Z_y

            # now that database has been updated, need to update omega and xi
            # NOTE: we process measurements, and then process movements

            l_index = 2 * (self.landmark_history[landmark_id]['index'] + 1)

            """take care of omega"""
            # update x-values
            self.omega[0][0] += 1. / self.measurement_noise
            self.omega[l_index][0] += -1. / self.measurement_noise
            self.omega[l_index][l_index] += 1. / self.measurement_noise
            self.omega[0][l_index] += -1. / self.measurement_noise

            # update y-values
            self.omega[1][1] += 1. / self.measurement_noise
            self.omega[1][l_index+1] += -1. / self.measurement_noise
            self.omega[l_index+1][1] += -1. / self.measurement_noise
            self.omega[l_index+1][l_index+1] += 1. / self.measurement_noise

            """take care of xi"""
            # update x-values
            self.xi[0][0] += -Z_x / self.measurement_noise
            self.xi[1][0] += -Z_y / self.measurement_noise
            self.xi[l_index][0] += Z_x / self.measurement_noise
            self.xi[l_index+1][0] += Z_y / self.measurement_noise

        # print("processing measurements!")

    ### NOTE: taken from drone.py ###
    def truncate_angle(self, t):
        """
        Truncate the angle between -PI and PI

        Args:
            t: angle to truncate.

        Returns:
            Truncated angle.
        """
        # return ((t + np.pi) % (2 * np.pi)) - np.pi
        return ((t + np.pi) % (2 * np.pi)) - np.pi
        # return (t % (2 * np.pi))

    ### END NOTE ###

    def process_movement(self, distance: float, steering: float):
        """
        Process a new movement and update (x,y) location of drone

        Args:
            distance: distance to move in meters
            steering: amount to turn in radians
        """
        self.robot_absolute_bearing = self.truncate_angle(self.robot_absolute_bearing + steering)
        # print("processing movements!")

        # convert polar coordinates to x and y movements in cartesian space
        dx = distance * np.cos(self.robot_absolute_bearing)
        dy = distance * np.sin(self.robot_absolute_bearing)

        # self.dx = dx
        # self.dy = dy

        # try passing in relative location instead
        # dx = distance * np.cos(steering)
        # dy = distance * np.sin(steering)

        self.x += dx
        self.y += dy

        # for each movement, add an additional two rows and columns, and copy
        # over the previous data from omega

        ### NOTE: copied from ps6_answers.py ###
        # expand info matrix and vector by one new position
        assert(self.omega.dimx == self.omega.dimy)  # needs to be a square matrix
        dim = self.omega.dimx
        idxs = [0,1] + list(range(4,dim+2))
        self.omega = self.omega.expand(dim+2, dim+2, idxs, idxs)
        self.xi = self.xi.expand(dim+2, 1, idxs, [0])
        ### END NOTE ###

        # add movement coefficients to omega
        # include all the x-values
        self.omega[0][0] += 1. / self.movement_noise
        self.omega[0][0+2] += -1. / self.movement_noise
        self.omega[0+2][0] += -1. / self.movement_noise
        self.omega[0+2][0+2] += 1. / self.movement_noise

        # include all the y-values
        self.omega[0+1][0+1] += 1. / self.movement_noise
        self.omega[0+1][0+3] += -1. / self.movement_noise
        self.omega[0+3][0+1] += -1. / self.movement_noise
        self.omega[0+3][0+3] += 1. / self.movement_noise

        # add coefficients to xi
        # include x-motions
        self.xi[0][0] += -dx / self.movement_noise
        self.xi[0+2][0] += dx / self.movement_noise

        # include y-motions
        self.xi[0+1][0] += -dy / self.movement_noise
        self.xi[0+3][0] += dy / self.movement_noise

        # now, just keep the latest position information
        ### NOTE: copied from ps6_answers.py ###
        newidxs = list(range(2, len(self.omega.value)))
        a = self.omega.take([0,1], newidxs)
        b = self.omega.take([0,1])
        c = self.xi.take([0,1], [0])
        self.omega = self.omega.take(newidxs) - a.transpose() * b.inverse() * a
        self.xi = self.xi.take(newidxs, [0]) - a.transpose() * b.inverse() * c
        ### END NOTE ###


class IndianaDronesPlanner:
    """
    Create a planner to navigate the drone to reach and extract the treasure marked by * from an unknown start position while avoiding obstacles (trees).
    """

    def __init__(self, max_distance: float, max_steering: float):
        """
        Initialize your planner here.

        Args:
            max_distance: the max distance the drone can travel in a single move in meters.
            max_steering: the max steering angle the drone can turn in a single move in radians.
        """
        self.max_distance = max_distance
        self.max_steering = max_steering
        self.slam = SLAM()

    ### NOTE: taken from drone.py ###
    def compute_distance(self, p, q):
        """Compute the distance between two points.

        Args:
            p: Point 1
            q: Point 2

        Returns:
            The Euclidean distance between the points.
        """

        x1, y1 = p
        x2, y2 = q

        dx = x2 - x1
        dy = y2 - y1

        return np.sqrt(dx ** 2 + dy ** 2)

    def compute_bearing(self, p, q):
        """
        Compute bearing between two points.

        Args:
            p: Point 1
            q: Point 2

        Returns:
            The bearing as referenced from the horizontal axis.
        """
        x1, y1 = p
        x2, y2 = q

        dx = x2 - x1
        dy = y2 - y1

        return math.atan2(dy, dx)
    ### END NOTE ###


    ### NOTE: from testing_suite_indiana_drones.py ###
    def line_circle_intersect(self, first_point, second_point, origin, radius):
        """ Checks if a line segment between two points intersects a circle of a certain radius and origin

        Args:
            first_point : (x,y)
            second_point : (x,y)
            origin : (x,y)
            radius : r

        Returns:
            intersect : True/False

        """

        ###REFERENCE###
        # https://math.stackexchange.com/questions/275529/check-if-line-intersects-with-circles-perimeter
        x1, y1 = first_point
        x2, y2 = second_point

        ox, oy = origin
        r = radius
        x1 -= ox
        y1 -= oy
        x2 -= ox
        y2 -= oy
        a = (x2 - x1) ** 2 + (y2 - y1) ** 2
        b = 2 * (x1 * (x2 - x1) + y1 * (y2 - y1))
        c = x1 ** 2 + y1 ** 2 - r ** 2
        disc = b ** 2 - 4 * a * c

        if a == 0:
            if c <= 0:
                return True
            else:
                return False
        else:

            if (disc <= 0):
                return False
            sqrtdisc = math.sqrt(disc)
            t1 = (-b + sqrtdisc) / (2 * a)
            t2 = (-b - sqrtdisc) / (2 * a)
            if ((0 < t1 and t1 < 1) or (0 < t2 and t2 < 1)):
                return True
            return False

    ### END NOTE ###

    def next_move(self, measurements: Dict, treasure_location: Dict):
        """Next move based on the current set of measurements.

        Args:
            measurements: Collection of measurements of tree positions and radius in the format 
                          {'landmark id':{'distance': float <meters>, 'bearing':float <radians>, 'type': char, 'radius':float <meters>}, ...}
            treasure_location: Location of Treasure in the format {'x': float <meters>, 'y':float <meters>, 'type': char '*'}
        
        Return: action: str, points_to_plot: dict [optional]
            action (str): next command to execute on the drone.
                allowed:
                    'move distance steering'
                    'move 1.0 1.570963'  - Turn left 90 degrees and move 1.0 distance.
                    
                    'extract treasure_type x_coordinate y_coordinate'
                    'extract * 1.5 -0.2' - Attempt to extract the treasure * from your current location (x = 1.5, y = -0.2).
                                           This will succeed if the specified treasure is within the minimum sample distance.
                   
            points_to_plot (dict): point estimates (x,y) to visualize if using the visualization tool [optional]
                            'self' represents the drone estimated position
                            <landmark_id> represents the estimated position for a certain landmark
                format:
                    {
                        'self': (x, y),
                        '<landmark_id_1>': (x1, y1),
                        '<landmark_id_2>': (x2, y2),
                        ....
                    }
        """
        action = ''
        goal_x = treasure_location['x']
        goal_y = treasure_location['y']
        distance = 0.0
        steering = 0.0

        self.slam.process_measurements(measurements)
        points_to_plot = self.slam.get_coordinates()
        robot_x, robot_y = points_to_plot['self']

        heading_to_goal = self.compute_bearing((robot_x, robot_y), (goal_x, goal_y))  # global
        distance_to_goal = self.compute_distance((robot_x, robot_y), (goal_x, goal_y))
        distance = distance_to_goal

        # compute desired steering / heading, from the robot's frame
        desired_steering = (heading_to_goal - self.slam.robot_absolute_bearing) # % (2 * np.pi)

        # print(f"DESIRED STEERING: {desired_steering}")
        # clamp heading and move distance if they are at maximum values
        if abs(desired_steering) > abs(self.max_steering):
            if desired_steering > 0:
                desired_steering = self.max_steering / 3
            else:
                desired_steering = -self.max_steering / 3
        else:
            steering = desired_steering
        if abs(distance_to_goal) > abs(self.max_distance):
            distance = self.max_distance / 7

        # now, iterate through all the trees in the horizon, and build a
        # safety bubble (threshold) for collision avoidance
        for landmark_id in measurements:
            tree_bearing = measurements[landmark_id]['bearing']
            tree_radius = measurements[landmark_id]['radius']
            distance_to_tree = measurements[landmark_id]['distance']

            dist_to_collision = distance_to_tree - tree_radius

            bearing_difference = desired_steering - tree_bearing
            positive_difference = bearing_difference > 0
            # if np.abs(bearing_difference) < (17 * np.pi/180) and dist_to_collision < 2.5*tree_radius:

            # TESTING - BZ #
            # try a while loop, where we compute x2, y2 of the projected move
            # and see if there's a crash. if there is, we continue to increment!
            #
            # origin of tree is estimated Lx, Ly which is in points to plot
            tree_origin_x = points_to_plot[landmark_id][0]
            tree_origin_y = points_to_plot[landmark_id][1]

            projected_x = distance * self.slam.truncate_angle(np.cos(desired_steering + self.slam.robot_absolute_bearing))
            projected_y = distance * self.slam.truncate_angle(np.sin(desired_steering + self.slam.robot_absolute_bearing))
            will_collide = self.line_circle_intersect((robot_x, robot_y), (projected_x, projected_y), (tree_origin_x, tree_origin_y), tree_radius)

            count = 0
            while will_collide:
                count += 1
                if positive_difference:
                    # desired_steering += (45 * np.pi / 180)
                    desired_steering += (2 * np.pi / 180)
                else:
                    desired_steering -= (2 * np.pi / 180)

                will_collide = self.line_circle_intersect((robot_x, robot_y), (projected_x, projected_y),
                                                          (tree_origin_x, tree_origin_y), tree_radius)

                if not will_collide:
                    print(f"Found an angle to prevent collision! It is {desired_steering}")

                if count > 10:
                    break
                # # END TEST #
                # if positive_difference:
                #     # desired_steering += (45 * np.pi / 180)
                #     desired_steering += (15 * np.pi / 180)
                # else:
                #     desired_steering -= (15 * np.pi / 180)
                # distance = self.max_distance / 15

        # # clamp heading and move distance if they are at maximum values
        # if abs(desired_steering) > abs(self.max_steering):
        #     if desired_steering > 0:
        #         steering = self.max_steering / 3
        #     else:
        #         steering = -self.max_steering / 3
        # else:
        #     steering = desired_steering
        # if abs(distance_to_goal) > abs(self.max_distance):
        #     distance = self.max_distance / 7
        # else:
        #     distance = distance_to_goal

        # check if we are within distance
        if distance_to_goal < 0.25:
            move_commanded = False  # AKA extract is commanded
        else:
            move_commanded = True

        if move_commanded:
            action = f'move {distance} {desired_steering}'
            self.slam.process_movement(distance, desired_steering)
        else:
            action = f'extract * {robot_x} {robot_y}'

        return action, points_to_plot

def who_am_i():
    # Please specify your GT login ID in the whoami variable (ex: jsmith222).
    whoami = 'bzhao301'
    return whoami
