######################################################################
# This file copyright the Georgia Institute of Technology
#
# Permission is given to students to use or modify this file (only)
# to work on their assignments.
#
# You may NOT publish this file or make it available to others not in
# the course.
#
######################################################################

# Optional: You may use deepcopy to help prevent aliasing
from copy import deepcopy

# You may use either the numpy library or Sebastian Thrun's matrix library for
# your matrix math in this project; uncomment the import statement below for
# the library you wish to use, and ensure that the library you are not using is
# commented out.
import numpy as np
# from matrix import matrix

# If you see different scores locally and on Gradescope this may be an
# indication that you are uploading a different file than the one you are
# executing locally. If this local ID doesn't match the ID on Gradescope then
# you uploaded a different file.
OUTPUT_UNIQUE_FILE_ID = False
if OUTPUT_UNIQUE_FILE_ID:
    import hashlib, pathlib
    file_hash = hashlib.md5(pathlib.Path(__file__).read_bytes()).hexdigest()
    print(f'Unique file ID: {file_hash}')


class Turret(object):
    """The laser used to defend against invading Meteorites."""

    def __init__(self, init_pos, max_angle_change,
                 dt):
        """Initialize the Turret."""
        self.x_pos = init_pos['x']
        self.y_pos = init_pos['y']
        self.max_angle_change = max_angle_change
        self.S_constant = 1. / 3.  # a constant
        self.dt = dt

        # store all meteorite-specific data in this master dictionary
        self.meteorites = {}  # contains state vector, P, Kalman Filter gain K

        # for the defense portion
        self.meteorites_prediction_tuple = None

        # state transition matrix, F
        # from Note on Deriving the F Matrix for Meteorites.pdf
        self.F = np.array([[1, 0, self.dt, 0, (self.dt**2)/2],
                           [0, 1, 0, self.dt, (self.S_constant * self.dt ** 2) / 2],
                           [0, 0, 1, 0, self.dt],
                           [0, 0, 0, 1, self.S_constant * self.dt],
                           [0, 0, 0, 0, 1]])

        # state mapping matrix, H
        self.H = np.array([[1, 0, 0, 0, 0],
                           [0, 1, 0, 0, 0]])

        # state dynamics uncertainty model, Q, set once and
        # used for all meteorites - may not be necessary
        var_y_Q = 0
        var_x_Q = 0
        var_y_d_Q = 0
        var_x_d_Q = 0
        var_y_dd_Q = 0
        self.Q = np.array([[var_y_Q, 0, 0, 0, 0],
                           [0, var_x_Q, 0, 0, 0],
                           [0, 0, var_y_d_Q, 0, 0],
                           [0, 0, 0, var_x_d_Q, 0],
                           [0, 0, 0, 0, var_y_dd_Q]])

        # observation uncertainty/noise, R
        # if measurements are independent, R will be a diagonal matrix
        var_z_x = 0.02
        var_z_y = 0.02
        self.R = np.array([[var_z_x, 0],
                           [0, var_z_y]])

        # state uncertainty model, P, generally
        # initialized as a diagonal matrix, will
        # be the same for every meteorite

        var_y_P_init = 100
        var_x_P_init = 100
        var_y_d_P_init = 25
        var_x_d_P_init = 25
        var_y_dd_P_init = 0.1
        self.P_init = np.array([[var_y_P_init, 0, 0, 0, 0],
                                [0, var_x_P_init, 0, 0, 0],
                                [0, 0, var_y_d_P_init, 0, 0],
                                [0, 0, 0, var_x_d_P_init, 0],
                                [0, 0, 0, 0, var_y_dd_P_init]])

        ####### defense-related parameters for turret #########
        self.firing_state = None  # FIRING or AIMING
        self.desired_heading = None  # [0, pi]

    def predict_from_observations(self, meteorite_observations):
        """Observe meteorite locations and predict their positions at time t+1.

        Parameters
        ----------
        self = a reference to the current object, the Turret
        meteorite_observations = a list of noisy observations of
            meteorite locations, taken at time t

        Returns
        -------
        A tuple or list of tuples containing (i, x, y), where i, x, and y are:
        i = the meteorite's ID
        x = the estimated x-coordinate of meteorite i's position for time t+1
        y = the estimated y-coordinate of meteorite i's position for time t+1

        Return format hint:
        For a tuple of tuples, this would look something like
        ((1, 0.4, 0.381), (2, 0.77, 0.457), ...)

        Notes
        -----
        Each observation in meteorite_observations is a tuple
        (i, x, y), where i is the unique ID for an meteorite, and x, y are the
        x, y locations (with noise) of the current observation of that
        meteorite at this timestep. Only meteorites that are currently
        'in-bounds' will appear in this list, so be sure to use the meteorite
        ID, and not the position/index within the list to identify specific
        meteorites.
        The list/tuple of tuples you return may change in size as meteorites
        move in and out of bounds.
        """
        # TODO: Update the Turret's estimate of where the meteorites are
        # located at the current timestep and return the updated estimates


        # x_estimate = np.array([[0, 0, 0, 0, 0]])

        N = len(meteorite_observations)

        ret_list = []  # convert to tuple later
        init_velocity_estimate_x = 0.1
        init_velocity_estimate_y = 0.1
        init_acceleration_estimate = 0.1

        # we need a kalman filter, individually tuned for each meteorite
        for i, observation in enumerate(meteorite_observations):
            id = observation[0]
            x = observation[1]
            y = observation[2]
            z = np.array([y, x])  # From kf-tune.pdf, Section 3

            if id not in self.meteorites:
                self.meteorites[id] = {}  # allocate dictionary for each meteorite
                self.meteorites[id]['x_estimate'] = np.array([y, x, init_velocity_estimate_x,
                                                              init_velocity_estimate_y, init_acceleration_estimate])
                self.meteorites[id]['P_estimate'] = self.P_init

            ####### MEASUREMENT UPDATE #################

            # Workflow followed from KF Tutorial.pdf
            # calculate innovation covariance
            self.meteorites[id]['S'] = self.H @ self.meteorites[id]['P_estimate'] @ self.H.transpose() + self.R

            # kalman gain
            self.meteorites[id]['K'] = self.meteorites[id]['P_estimate'] @ self.H.transpose() @ np.linalg.inv(self.meteorites[id]['S'])  # From kf-tune.pdf

            # innovation or residual
            innov = z - (self.H @ self.meteorites[id]['x_estimate'])

            # update state based on residual
            self.meteorites[id]['x_estimate'] = self.meteorites[id]['x_estimate'] + self.meteorites[id]['K'] @ innov

            # then, update kalman filter's estimate of uncertainty  # From KF Tutorial.pdf
            self.meteorites[id]['P_estimate'] = self.meteorites[id]['P_estimate'] - (self.meteorites[id]['K'] @ self.H @ self.meteorites[id]['P_estimate'])


            ##### MOTION UPDATE / PREDICTION STEP #####
            # predict the state x at time t
            self.meteorites[id]['x_estimate'] = self.F @ self.meteorites[id]['x_estimate']

            # using previous uncertainty, predict uncertainty P at time t
            self.meteorites[id]['P_estimate'] = self.F @ self.meteorites[id]['P_estimate'] @ self.F.transpose() + self.Q

            ret_list.append([id, self.meteorites[id]['x_estimate'][1], self.meteorites[id]['x_estimate'][0]])

        ret_tuple = tuple(map(tuple, ret_list))

        self.meteorites_prediction_tuple = ret_tuple

        return ret_tuple



## ATTEMPT NUMBER 2 ##
    def get_laser_action(self, current_aim_rad):
        """Return the laser's action; it can change its aim angle or fire.

                Parameters
                ----------
                self = a reference to the current object, the Turret
                current_aim_rad = the laser turret's current aim angle, in radians,
                    provided by the simulation.


                Returns
                -------
                Float (desired change in laser aim angle, in radians), OR
                    String 'fire' to fire the laser

                Notes
                -----
                The laser can aim in the range [0.0, pi].

                The maximum amount the laser's aim angle can change in a given timestep
                is self.max_angle_change radians. Larger change angles will be
                clamped to self.max_angle_change, but will keep the same sign as the
                returned desired angle change (e.g. an angle change of -3.0 rad would
                be clamped to -self.max_angle_change).

                If the laser is aimed at 0.0 rad, it will point horizontally to the
                right; if it is aimed at pi rad, it will point to the left.

                If the value returned from this function is the string 'fire' instead
                of a numerical angle change value, the laser will fire instead of
                moving.
                """
        # TODO: Update the change in the laser aim angle, in radians, based
        # on where the meteorites are currently, OR return 'fire' to fire the
        # laser at a meteorite

        min_y_threshold = -0.9
        min_y = 1.0
        min_x = 1.0

        # turret coordinates
        x0 = 0
        y0 = -1

        for meteorite in self.meteorites_prediction_tuple:
            if meteorite[0] is -1:
                continue

            # skip the really fast meteors
            if self.meteorites[meteorite[0]]['x_estimate'][4] > 0.08:
                continue

            y = meteorite[2]
            x = meteorite[1]

            if y < min_y_threshold:
                continue

            if y < min_y:
                min_y = y
                min_x = x

        delta_y = min_y - y0
        delta_x = min_x - x0
        self.desired_heading = np.arctan2(delta_y, delta_x)
        # print("delta y:", delta_y)
        # print("delta x:", delta_x)
        # print("Desired heading:", self.desired_heading, "arctan2:", np.arctan2(delta_y, delta_x))
        # print("Current aim:", current_aim_rad)

        is_in_range_y = abs(delta_y) < abs(1.1 * np.sin(self.desired_heading))
        is_in_range_x = abs(delta_x) < abs(1.1 * np.cos(self.desired_heading))

        if abs(self.desired_heading - current_aim_rad) < 0.015 and is_in_range_y and is_in_range_x:
            return 'fire'
        else:
            # print("Where is the fn going?")
            return (self.desired_heading - current_aim_rad)


    # def get_laser_action(self, current_aim_rad):
    #     """Return the laser's action; it can change its aim angle or fire.
    #
    #     Parameters
    #     ----------
    #     self = a reference to the current object, the Turret
    #     current_aim_rad = the laser turret's current aim angle, in radians,
    #         provided by the simulation.
    #
    #
    #     Returns
    #     -------
    #     Float (desired change in laser aim angle, in radians), OR
    #         String 'fire' to fire the laser
    #
    #     Notes
    #     -----
    #     The laser can aim in the range [0.0, pi].
    #
    #     The maximum amount the laser's aim angle can change in a given timestep
    #     is self.max_angle_change radians. Larger change angles will be
    #     clamped to self.max_angle_change, but will keep the same sign as the
    #     returned desired angle change (e.g. an angle change of -3.0 rad would
    #     be clamped to -self.max_angle_change).
    #
    #     If the laser is aimed at 0.0 rad, it will point horizontally to the
    #     right; if it is aimed at pi rad, it will point to the left.
    #
    #     If the value returned from this function is the string 'fire' instead
    #     of a numerical angle change value, the laser will fire instead of
    #     moving.
    #     """
    #     # TODO: Update the change in the laser aim angle, in radians, based
    #     # on where the meteorites are currently, OR return 'fire' to fire the
    #     # laser at a meteorite
    #     min_y_threshold = -0.8
    #     lowest_current_y = 1.0
    #     lowest_current_x = 1.0
    #
    #     turret_pos = np.array([0, -1])
    #
    #
    #     for i, meteor_id in enumerate(self.meteorites):
    #
    #         if meteor_id is -1:
    #             continue
    #
    #         X = deepcopy(self.meteorites[meteor_id]['x_estimate'])
    #         y = X[0]
    #         x = X[1]
    #         y_dot = X[2]
    #         x_dot = X[3]
    #         accel = X[4]
    #         if accel > 3.0:
    #             # too fast of an acceleration, so skip this meteor
    #             continue
    #
    #         if y < min_y_threshold:
    #             continue
    #
    #         delta_y = (y - turret_pos[1])
    #         delta_x = (x - turret_pos[0])
    #         self.desired_heading = np.deg2rad(np.arctan2(delta_y, delta_x))
    #
    #         is_in_range_y = delta_y < (1.1 * np.sin(self.desired_heading))
    #         is_in_range_x = delta_x < (1.1 * np.cos(self.desired_heading))
    #         if abs(self.desired_heading - current_aim_rad) < 0.01 and is_in_range_y and is_in_range_x:
    #             return 'fire'
    #
    #         if y < lowest_current_y:
    #             lowest_current_y = y
    #             lowest_current_x = x
    #
    #     # now that we've looped through meteorites and found
    #     # the lowest one, aim in that direction
    #
    #     delta_y = (lowest_current_y - turret_pos[1])
    #     delta_x = (lowest_current_x - turret_pos[0])
    #
    #     self.desired_heading = np.deg2rad(np.arctan2(delta_y, delta_x))
    #     is_in_range_y = delta_y < (1.1 * np.sin(self.desired_heading))
    #     is_in_range_x = delta_x < (1.1 * np.cos(self.desired_heading))
    #
    #     if abs(self.desired_heading - current_aim_rad) < 0.01 and is_in_range_y and is_in_range_x:
    #         return 'fire'
    #     else:
    #         return (self.desired_heading - current_aim_rad)
    #
    #     # return 0.0  # or 'fire'


def who_am_i():
    # Please specify your GT login ID in the whoami variable (ex: jsmith222).
    whoami = 'bzhao301'
    return whoami
