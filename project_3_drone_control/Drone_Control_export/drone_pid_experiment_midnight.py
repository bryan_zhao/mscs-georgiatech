######################################################################
# This file copyright the Georgia Institute of Technology
#
# Permission is given to students to use or modify this file (only)
# to work on their assignments.
#
# You may NOT publish this file or make it available to others not in
# the course.
#
######################################################################

def is_same_sign(error, controller_output) -> bool:
    '''
    Checks if the error term and controller output are the same sign
    for dealing with integrator windup.
    '''
    if error > 0 and controller_output > 0:
        return True
    if error < 0 and controller_output < 0:
        return True
    return False


def pid_thrust(target_elevation, drone_elevation, tau_p=0.0, tau_d=0.0, tau_i=0.0, data: dict() = {}):
    '''
    Student code for Thrust PID control. Drone's starting x, y position is (0, 0).

    Args:
    target_elevation: The target elevation that the drone has to achieve
    drone_elevation: The drone's elevation at the current time step
    tau_p: Proportional gain
    tau_i: Integral gain
    tau_d: Differential gain
    data: Dictionary that you can use to pass values across calls.
        Reserved keys:
            max_rpm_reached: (True|False) - Whether Drone has reached max RPM in both its rotors.

    Returns:
        Tuple of thrust, data
        thrust - The calculated thrust using PID controller
        data - A dictionary containing any values you want to pass to the next
            iteration of this function call.
            Reserved keys:
                max_rpm_reached: (True|False) - Whether Drone has reached max RPM in both its rotors.
    '''

    err = target_elevation - drone_elevation  # in y coordinates

    # If the data dictionary hasn't been populated, initialize it with zeroes
    if not data.get('accumulated_error', False):
        data['accumulated_error'] = 0.0

    if not data.get('previous_error', False):
        data['previous_error'] = err

    # Compute I and D error terms
    data['accumulated_error'] += err

    diff = err - data['previous_error']
    thrust = (tau_p * err + tau_i * data['accumulated_error']
              + tau_d * diff)  # control input

    # integrator anti-windup implementation:
    # decide to clamp based on whether 1) the output is saturating and 2) the error is same sign
    # as the controller output
    if not data.get('max_rpm_reached', False):
        data['max_rpm_reached'] = False

    if data['max_rpm_reached'] and is_same_sign(err, thrust):
        # saturation has occurred, unwind integral term
        data['accumulated_error'] = 0.0

    data['previous_error'] = err

    return thrust, data


def pid_roll(target_x, drone_x, tau_p=0, tau_d=0, tau_i=0, data: dict() = {}):
    '''
    Student code for PD control for roll. Drone's starting x,y position is 0, 0.

    Args:
    target_x: The target horizontal displacement that the drone has to achieve
    drone_x: The drone's x position at this time step
    tau_p: Proportional gain, supplied by the test suite
    tau_i: Integral gain, supplied by the test suite
    tau_d: Differential gain, supplied by the test suite
    data: Dictionary that you can use to pass values across calls.

    Returns:
        Tuple of roll, data
        roll - The calculated roll using PID controller
        data - A dictionary containing any values you want to pass to the next
            iteration of this function call.

    '''

    err = target_x - drone_x  # in x coordinates

    # If the data dictionary hasn't been populated, initialize it with zeroes
    if not data.get('accumulated_error', False):
        data['accumulated_error'] = 0.0

    if not data.get('previous_error', False):
        data['previous_error'] = err

    # Compute I and D error terms
    data['accumulated_error'] += err
    diff = err - data['previous_error']
    roll = (tau_p * err + tau_i * data['accumulated_error']
            + tau_d * diff)  # control input

    data['previous_error'] = err

    return roll, data


def find_parameters_thrust(run_callback, tune='thrust', DEBUG=False, VISUALIZE=False):
    '''
    Student implementation of twiddle algorithm will go here. Here you can focus on
    tuning gain values for Thrust test cases only.

    Args:
    run_callback: A handle to DroneSimulator.run() method. You should call it with your
                PID gain values that you want to test with. It returns an error value that indicates
                how well your PID gain values followed the specified path.

    tune: This will be passed by the test harness.
            A value of 'thrust' means you only need to tune gain values for thrust.
            A value of 'both' means you need to tune gain values for both thrust and roll.

    DEBUG: Whether or not to output debugging statements during twiddle runs
    VISUALIZE: Whether or not to output visualizations during twiddle runs

    Returns:
        tuple of the thrust_params, roll_params:
            thrust_params: A dict of gain values for the thrust PID controller
              thrust_params = {'tau_p': 0.0, 'tau_d': 0.0, 'tau_i': 0.0}

            roll_params: A dict of gain values for the roll PID controller
              roll_params   = {'tau_p': 0.0, 'tau_d': 0.0, 'tau_i': 0.0}

    '''

    # Initialize a list to contain your gain values that you want to tune
    # params = [0.3, 0.05, 0.001]
    params = [0.3, 20, 0.000]
    dp = [1, 1, 0]
    tol = 0.001

    # Create dicts to pass the parameters to run_callback
    thrust_params = {'tau_p': params[0], 'tau_d': params[1], 'tau_i': params[2]}

    # If tuning roll, then also initialize gain values for roll PID controller
    roll_params = {'tau_p': 0, 'tau_d': 0, 'tau_i': 0}

    # Call run_callback, passing in the dicts of thrust and roll gain values
    hover_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
        thrust_params, roll_params, VISUALIZE=VISUALIZE)

    # Calculate best_error from above returned values
    best_error = hover_error

    # Implement your code to use twiddle to tune the params and find the best_error
    exceeded_velocity_penalty = 10
    exceeded_oscillation_penalty = 10
    hover_error_threshold = 0.5

    # Start: taken from lecture notes, L5_PID.py (my own implementation)
    while sum(dp) > tol:  # or hover_error >= hover_error_threshold:
        for i in range(len(params)):
            # go through each parameter sequentially
            # first, try to increase params by dp
            params[i] += dp[i]
            thrust_params = {'tau_p': params[0], 'tau_d': params[1], 'tau_i': params[2]}
            hover_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
                thrust_params, roll_params, VISUALIZE=VISUALIZE)

            # penalize exceeding max velocity and max number of oscillations
            if drone_max_velocity > max_allowed_velocity:
                hover_error += exceeded_velocity_penalty * abs(drone_max_velocity - max_allowed_velocity)
            if total_oscillations > max_allowed_oscillations:
                hover_error += exceeded_oscillation_penalty * abs(total_oscillations - max_allowed_oscillations)

            if hover_error < best_error:
                # adjust dp in positive direction
                best_error = hover_error
                dp[i] *= 1.1  # increment upwards
            else:
                # adjust dp in negative direction
                params[i] -= 2.0 * dp[i]  # have to do twice, bc added it before
                thrust_params = {'tau_p': params[0], 'tau_d': params[1], 'tau_i': params[2]}
                hover_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
                    thrust_params, roll_params, VISUALIZE=VISUALIZE)

                # penalize exceeding max velocity and max number of oscillations
                if drone_max_velocity > max_allowed_velocity:
                    hover_error += exceeded_velocity_penalty * abs(drone_max_velocity - max_allowed_velocity)
                if total_oscillations > max_allowed_oscillations:
                    hover_error += exceeded_oscillation_penalty * abs(total_oscillations - max_allowed_oscillations)

                if hover_error < best_error:
                    best_error = hover_error
                    dp[i] *= 1.1
                else:
                    # incrementing by 1.0 or larger has failed, so resort to decreasing dp
                    params[i] += dp[i]  # reset p[i] to original since both failed above
                    dp[i] *= 0.9  # now, increment by 0.9 instead
    # End: taken from lecture notes, L5_PID.py (my own implementation)

    # Return the dict of gain values that give the best error.

    return thrust_params, roll_params


def find_parameters_with_int(run_callback, tune='thrust', DEBUG=False, VISUALIZE=False):
    '''
    Student implementation of twiddle algorithm will go here. Here you can focus on
    tuning gain values for Thrust test case with Integral error

    Args:
    run_callback: A handle to DroneSimulator.run() method. You should call it with your
                PID gain values that you want to test with. It returns an error value that indicates
                how well your PID gain values followed the specified path.

    tune: This will be passed by the test harness.
            A value of 'thrust' means you only need to tune gain values for thrust.
            A value of 'both' means you need to tune gain values for both thrust and roll.

    DEBUG: Whether or not to output debugging statements during twiddle runs
    VISUALIZE: Whether or not to output visualizations during twiddle runs

    Returns:
        tuple of the thrust_params, roll_params:
            thrust_params: A dict of gain values for the thrust PID controller
              thrust_params = {'tau_p': 0.0, 'tau_d': 0.0, 'tau_i': 0.0}

            roll_params: A dict of gain values for the roll PID controller
              roll_params   = {'tau_p': 0.0, 'tau_d': 0.0, 'tau_i': 0.0}

    '''

    # Initialize a list to contain your gain values that you want to tune
    params = [8, 50, 0.001]
    dp = [1, 1, 1]
    tol = 0.01
    hover_error_threshold = 0.05

    # Create dicts to pass the parameters to run_callback
    thrust_params = {'tau_p': params[0], 'tau_d': params[1], 'tau_i': params[2]}

    # If tuning roll, then also initialize gain values for roll PID controller
    roll_params = {'tau_p': 0, 'tau_d': 0, 'tau_i': 0}

    # Call run_callback, passing in the dicts of thrust and roll gain values
    best_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
        thrust_params, roll_params, VISUALIZE=VISUALIZE)

    # Calculate best_error from above returned values

    # Implement your code to use twiddle to tune the params and find the best_error
    # weighted error factors
    exceeded_velocity_penalty = 1
    exceeded_oscillation_penalty = 1

    # Start: taken from lecture notes, L5_PID.py (my own implementation)
    while sum(dp) > tol or hover_error > hover_error_threshold:
        for i in range(len(params)):
            # go through each parameter sequentially
            # first, try to increase params by dp
            params[i] += dp[i]
            thrust_params = {'tau_p': params[0], 'tau_d': params[1], 'tau_i': params[2]}
            hover_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
                thrust_params, roll_params, VISUALIZE=VISUALIZE)

            # penalize exceeding max velocity and max number of oscillations
            if drone_max_velocity > max_allowed_velocity:
                hover_error += exceeded_velocity_penalty * abs(drone_max_velocity - max_allowed_velocity)
            if total_oscillations > max_allowed_oscillations:
                hover_error += exceeded_oscillation_penalty * abs(total_oscillations - max_allowed_oscillations)

            if hover_error < best_error:
                # adjust dp in positive direction
                best_error = hover_error
                dp[i] *= 1.1  # increment upwards
            else:
                # adjust dp in negative direction
                params[i] -= 2.0 * dp[i]  # have to do twice, bc added it before
                thrust_params = {'tau_p': params[0], 'tau_d': params[1], 'tau_i': params[2]}
                hover_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
                    thrust_params, roll_params, VISUALIZE=VISUALIZE)

                # penalize exceeding max velocity and max number of oscillations
                if drone_max_velocity > max_allowed_velocity:
                    hover_error += exceeded_velocity_penalty * abs(drone_max_velocity - max_allowed_velocity)
                if total_oscillations > max_allowed_oscillations:
                    hover_error += exceeded_oscillation_penalty * abs(total_oscillations - max_allowed_oscillations)

                if hover_error < best_error:
                    best_error = hover_error
                    dp[i] *= 1.1
                else:
                    # incrementing by 1.0 or larger has failed, so resort to decreasing dp
                    params[i] += dp[i]  # reset p[i] to original since both failed above
                    dp[i] *= 0.9  # now, increment by 0.9 instead
    # End: taken from lecture notes, L5_PID.py (my own implementation)

    # Return the dict of gain values that give the best error.

    return thrust_params, roll_params


def find_parameters_with_roll(run_callback, tune='both', DEBUG=False, VISUALIZE=False):
    '''
    Student implementation of twiddle algorithm will go here. Here you will
    find gain values for Thrust as well as Roll PID controllers.

    Args:
    run_callback: A handle to DroneSimulator.run() method. You should call it with your
                PID gain values that you want to test with. It returns an error value that indicates
                how well your PID gain values followed the specified path.

    tune: This will be passed by the test harness.
            A value of 'thrust' means you only need to tune gain values for thrust.
            A value of 'both' means you need to tune gain values for both thrust and roll.

    DEBUG: Whether or not to output debugging statements during twiddle runs
    VISUALIZE: Whether or not to output visualizations during twiddle runs

    Returns:
        tuple of the thrust_params, roll_params:
            thrust_params: A dict of gain values for the thrust PID controller
              thrust_params = {'tau_p': 0.0, 'tau_d': 0.0, 'tau_i': 0.0}

            roll_params: A dict of gain values for the roll PID controller
              roll_params   = {'tau_p': 0.0, 'tau_d': 0.0, 'tau_i': 0.0}

    '''
    # Initialize a list to contain your thrust gain values that you want to tune, e.g.,
    params_thrust_input = [0.23, 20, 0.000]
    dp_thrust = [1, 1, 0]
    tol_thrust = 0.01

    # Create dicts to pass the parameters to run_callback
    thrust_params = {'tau_p': params_thrust_input[0], 'tau_d': params_thrust_input[1], 'tau_i': params_thrust_input[2]}

    # If tuning roll, then also initialize gain values for roll PID controller
    roll_params = {'tau_p': 0, 'tau_d': 0, 'tau_i': 0}

    # Call run_callback, passing in the dicts of thrust and roll gain values
    best_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
        thrust_params, roll_params, VISUALIZE=VISUALIZE)

    exceeded_velocity_penalty = 0.1
    exceeded_oscillation_penalty = 10

    # Implement your code to use twiddle to tune the params and find the best_error
    while sum(dp_thrust) > tol_thrust:
        for i in range(len(params_thrust_input)):
            # go through each parameter sequentially
            # first, try to increase params by dp_thrust
            params_thrust_input[i] += dp_thrust[i]
            thrust_params = {'tau_p': params_thrust_input[0], 'tau_d': params_thrust_input[1],
                             'tau_i': params_thrust_input[2]}
            hover_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
                thrust_params, roll_params, VISUALIZE=VISUALIZE)

            # penalize exceeding max velocity and max number of oscillations
            if drone_max_velocity > max_allowed_velocity:
                hover_error += exceeded_velocity_penalty * abs(drone_max_velocity - max_allowed_velocity)
            if total_oscillations > max_allowed_oscillations:
                hover_error += exceeded_oscillation_penalty * abs(total_oscillations - max_allowed_oscillations)

            if hover_error < best_error:
                # adjust dp_thrust in positive direction
                best_error = hover_error
                dp_thrust[i] *= 1.1  # increment upwards
            else:
                # adjust dp_thrust in negative direction
                params_thrust_input[i] -= 2.0 * dp_thrust[i]  # have to do twice, bc added it before
                thrust_params = {'tau_p': params_thrust_input[0], 'tau_d': params_thrust_input[1],
                                 'tau_i': params_thrust_input[2]}
                hover_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
                    thrust_params, roll_params, VISUALIZE=VISUALIZE)

                # penalize exceeding max velocity and max number of oscillations
                if drone_max_velocity > max_allowed_velocity:
                    hover_error += exceeded_velocity_penalty * abs(drone_max_velocity - max_allowed_velocity)
                if total_oscillations > max_allowed_oscillations:
                    hover_error += exceeded_oscillation_penalty * abs(total_oscillations - max_allowed_oscillations)

                if hover_error < best_error:
                    best_error = hover_error
                    dp_thrust[i] *= 1.1
                else:
                    # incrementing by 1.0 or larger has failed, so resort to decreasing dp_thrust
                    params_thrust_input[i] += dp_thrust[i]  # reset p[i] to original since both failed above
                    dp_thrust[i] *= 0.9  # now, increment by 0.9 instead

    # Now, tune the roll parameters in sequence
    # Initialize a list to contain your roll gain values that you want to tune, e.g.,
    params_roll_input = [0.2, 0.9, 0]
    dp_roll = [1, 1, 0]
    tol_roll = 0.01

    # Implement your code to use twiddle to tune the params and find the best_error
    while sum(dp_roll) > tol_roll:
        for i in range(len(params_roll_input)):
            # go through each parameter sequentially
            # first, try to increase params by dp_roll
            params_roll_input[i] += dp_roll[i]
            roll_params = {'tau_p': params_roll_input[0], 'tau_d': params_roll_input[1], 'tau_i': params_roll_input[2]}
            hover_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
                thrust_params, roll_params, VISUALIZE=VISUALIZE)
            if hover_error < best_error:
                # adjust dp_roll in positive direction
                best_error = hover_error
                dp_roll[i] *= 1.1  # increment upwards
            else:
                # adjust dp_roll in negative direction
                params_roll_input[i] -= 2.0 * dp_roll[i]  # have to do twice, bc added it before
                roll_params = {'tau_p': params_roll_input[0], 'tau_d': params_roll_input[1],
                               'tau_i': params_roll_input[2]}
                hover_error, max_allowed_velocity, drone_max_velocity, max_allowed_oscillations, total_oscillations = run_callback(
                    thrust_params, roll_params, VISUALIZE=VISUALIZE)
                if hover_error < best_error:
                    best_error = hover_error
                    dp_roll[i] *= 1.1
                else:
                    # incrementing by 1.0 or larger has failed, so resort to decreasing dp_roll
                    params_roll_input[i] += dp_roll[i]  # reset p[i] to original since both failed above
                    dp_roll[i] *= 0.9  # now, increment by 0.9 instead

    # End: taken from lecture notes, L5_PID.py (my own implementation)

    # Return the dict of gain values that give the best error.

    return thrust_params, roll_params


def who_am_i():
    # Please specify your GT login ID in the whoami variable (ex: jsmith222).
    whoami = 'bzhao301'
    return whoami
